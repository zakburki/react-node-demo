// Node Dependencies
var express = require('express'),
  exphbs = require('express-handlebars'),
  http = require('http'),
  mongoose = require('mongoose'),
  twitter = require('ntwitter'),
  routes = require('./routes'),
  config = require('./config'),
  streamHandler = require('./utils/streamHandler');


//Create App and Configure
var app = express();
var port = process.env.PORT || 8080;
app.engine('handlebars', exphbs({ defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.disable('etag');
mongoose.connect('mongodb://localhost/react-node');

//Create a new twit package instance
var twit = new twitter(config.twitter);

//Express Routes
app.get('/', routes.index);
app.get('/page/:page/:skip', routes.page);
app.use("/", express.static(__dirname + "/public/"));

//Initialise Server.
var server = http.createServer(app).listen(port, function() {
  console.log('Express server listening on port ' + port);
});

//Initialise socket.io
var io = require('socket.io').listen(server);

//Set a twitter stream for track.
twit.stream('statuses/filter',{track: 'HKJS'}, function(stream){
  streamHandler(stream,io);
});